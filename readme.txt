Before running npm run migrate for a second time
The phone_calls keyspace needs to be cleaned. Run:

path-to-grakn-dist-directory/graql console -k phone_calls
clean
confirm
exit
path-to-grakn-dist-directory/graql console -k phone_calls -f path-to-cloned-repository/nodejs/migration/schema.gql