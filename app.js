const Grakn = require("grakn");
const fs = require("fs");

// for creating custom JSON processing pipelines with a minimal memory footprint
// https://github.com/uhop/stream-json
const { parser } = require("stream-json");
// to stream out assembles objects, assuming an array of objects in the JSON file
// https://github.com/uhop/stream-json/wiki/StreamArray
const { streamArray } = require("stream-json/streamers/StreamArray");
// used in conjunction with stream-json to simplify data processing
// https://github.com/uhop/stream-chain
const { chain } = require("stream-chain");

const inputs = [
  { dataPath: "./data/companies", template: companyTemplate },
  { dataPath: "./data/devices", template: deviceTemplate },
  { dataPath: "./data/production", template: productionTemplate }
];

// Go
buildGraph(inputs);

/**
 * gets the job done:
 * 1. creates a Grakn instance
 * 2. creates a session to the targeted keyspace
 * 3. loads csv to Grakn for each file
 * 4. closes the session
 */
async function buildGraph() {
  const grakn = new Grakn("localhost:48555"); // 1
  const session = grakn.session("devices"); // 2

  for (input of inputs) {
    console.log("Loading from [" + input.dataPath + "] into Grakn ...");
    await loadDataIntoGrakn(input, session); // 3
  }

  session.close(); // 4
}

/**
 * loads the csv data into our Grakn phone_calls keyspace
 * @param {object} input contains details required to parse the data
 * @param {object} session a Grakn session, off of which a tx will be created
 */
async function loadDataIntoGrakn(input, session) {
  const items = await parseDataToObjects(input);

  for (item of items) {
    const tx = await session.transaction(Grakn.txType.WRITE);

    const graqlInsertQuery = input.template(item);
    console.log("Executing Graql Query: " + graqlInsertQuery);
    //added await keyword for synchronous insertion
    await tx.query(graqlInsertQuery);
    await tx.commit();
  }

  console.log(
    `\nInserted ${items.length} items from [${input.dataPath}] into Grakn.\n`
  );
}

function companyTemplate(company) {
  return `insert $company isa company has name "${company.name}";`;
}

function deviceTemplate(device) {
  const { model, category } = device;
  return `insert $device isa device has model "${model}" has category "${category}"; `;
}

function productionTemplate(production) {
  const { company_name, device_name } = production;
  let graqlInsertQuery = `match $company isa company has name "${company_name}"; `;
  graqlInsertQuery += `$product isa device has model "${device_name}"; `;
  graqlInsertQuery += "insert (manufacturer: $company, product: $product) isa production;";
  return graqlInsertQuery;
}


/**
 * 1. reads the file through a stream,
 * 2. adds the  object to the list of items
 * @param {string} input.dataPath path to the data file
 * @returns items that is, a list of objects each representing a data item
 */
function parseDataToObjects(input) {
  const items = [];
  return new Promise(function (resolve, reject) {
    const pipeline = chain([
      fs.createReadStream(input.dataPath + ".json"), // 1
      parser(),
      streamArray()
    ]);

    // 2
    pipeline.on("data", function (result) {
      items.push(result.value); // 2
    });

    pipeline.on("end", function () {
      resolve(items);
    });
  });
}
